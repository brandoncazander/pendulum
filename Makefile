#!/bin/make
.PHONY: all install install-systemd clean

all:
	@echo "Run 'make install' to install pendulum"
	@echo "Run 'make install-systemd' to install systemd service and timer."

install:
	python3 -m pip install -r requirements.pip --user	
	python3 setup.py install --user

install-systemd:
	install systemd/pendulum.service /lib/systemd/system/pendulum.service
	install systemd/pendulum.timer /lib/systemd/system/pendulum.timer
	systemctl enable pendulum.timer

clean:
	rm -rf dist/ build/ pendulum.egg-info/
