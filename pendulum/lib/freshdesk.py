import json

import requests

HEADERS = {'Content-Type': 'application/json'}


class Freshdesk:
    def __init__(self, domain, token, agent_id):
        self.domain = domain
        self.token = token
        self.agent_id = int(agent_id)

    @property
    def auth(self):
        return (self.token, 'api_token')

    @property
    def params(self):
        return {
            'headers': HEADERS,
            'auth': self.auth,
        }

    def post(self, ticket_id, note, start_time, time_spent, billable=False):
        response = requests.post(
            url='https://{domain}/api/v2/tickets/{ticket_id}/time_entries'.format(
                domain=self.domain,
                ticket_id=ticket_id,
            ),
            data=json.dumps({
                'agent_id': self.agent_id,
                'note': note,
                'executed_at': start_time,
                'time_spent': time_spent,
                'timer_running': False,
                'billable': billable,
            }),
            **self.params
        )
        response.raise_for_status()
        return response.content
