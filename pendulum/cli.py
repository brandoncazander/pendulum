#!/usr/bin/env python3
import argparse
import atexit
import datetime
import math
import sys
import traceback

import colorama
import pytz
from slackclient import SlackClient

import configparser
from pendulum.lib.toggl import Toggl
from pendulum.lib.jira import Jira
from pendulum.lib.freshdesk import Freshdesk

CONFIG_FILE = '/etc/pendulum.conf'
POSTED = '{colour}✔{reset}'.format(colour=colorama.Fore.GREEN, reset=colorama.Style.RESET_ALL)
NOT_POSTED = '{colour}❌{reset}'.format(colour=colorama.Fore.RED, reset=colorama.Style.RESET_ALL)
ERROR = '{colour}❗{reset}'.format(colour=colorama.Fore.RED, reset=colorama.Style.RESET_ALL)
TZ_INFO = pytz.timezone('America/Vancouver')


class Logger:
    def __init__(self):
        self.log_messages = []
        self.slack_token = None
        self.slack_channel = None
        self.slack = None
        self.last_end = None

    def init_slack(self, token, channel):
        self.slack_token = token
        self.slack_channel = channel
        self.slack = SlackClient(self.slack_token)

    def log(self, msg, end=None):
        orig_msg = msg
        if self.last_end is not None:
            msg = self.log_messages.pop() + msg
        if end is not None:
            self.last_end = end
            msg += end
        else:
            self.last_end = None

        self.log_messages.append(msg)
        print(orig_msg, end=end)

    def on_exit(self):
        if self.slack:
            self.slack.api_call(
                "chat.postMessage",
                username="Pendulum",
                channel=self.slack_channel,
                text="\n".join(self.log_messages),
            )


logger = Logger()
atexit.register(logger.on_exit)


def main():
    colorama.init()
    parser = argparse.ArgumentParser(description='Post time from toggl to JIRA and Freshdesk')
    parser.add_argument('--review', action='store_true', help='Review entries without posting')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--dates', nargs=1, help='Dates to post entries from in YYYY-MM-DD, comma-separated')
    group.add_argument('--range', nargs=2, help='Date range to post entries from in YYYY-MM-DD')
    args = parser.parse_args()
    freshdesk_enabled = True

    config = configparser.ConfigParser()
    config.read(CONFIG_FILE)

    try:
        logger.init_slack(
            config.get('slack', 'token'),
            config.get('slack', 'channel'),
        )
    except configparser.NoSectionError:
        pass

    try:
        freshdesk = Freshdesk(
            domain=config.get('freshdesk', 'domain'),
            token=config.get('freshdesk', 'token'),
            agent_id=config.get('freshdesk', 'agent_id')
        )
    except configparser.NoSectionError:
        freshdesk_enabled = False

    try:
        try:
            toggl = Toggl(
                token=config.get('toggl', 'api_token'),
                workspace_id=config.get('toggl', 'workspace_id'),
                freshdesk_enabled=freshdesk_enabled,
            )
        except configparser.NoSectionError as e:
            logger.log("Invalid configuration file. Could not read Toggl settings: %s" % e)
            sys.exit(1)

        try:
            jira = Jira(
                server=config.get('jira', 'server'),
                username=config.get('jira', 'username'),
                password=config.get('jira', 'password')
            )
        except configparser.NoSectionError as e:
            logger.log("Invalid configuration file. Could not read JIRA settings: %s" % e)
            sys.exit(1)

        def format_date(datestring):
            date = datetime.datetime.strptime(datestring, '%Y-%m-%d')
            return TZ_INFO.localize(datetime.datetime(date.year, date.month, date.day, 23, 59, 59))

        dates = []
        if args.dates:
            for datestring in args.dates[0].split(','):
                dates.append(format_date(datestring))
        elif args.range:
            start = format_date(args.range[0])
            end = format_date(args.range[1])
            dates = [start + datetime.timedelta(days=x) for x in range(0, (end-start).days + 1)]
        else:
            date = datetime.date.today() - datetime.timedelta(days=1)
            dates.append(TZ_INFO.localize(datetime.datetime(date.year, date.month, date.day, 23, 59, 59)))

        for date in dates:
            logger.log('Retrieving entries for {date}'.format(date=date.strftime('%Y-%m-%d')))
            entries = toggl.get_entries(date=date)

            try:
                non_running_entries = [entry for entry in entries if not entry.running]
                total_duration = sum([entry.duration for entry in non_running_entries]) if non_running_entries else 1
                admin_percentage = sum([entry.duration for entry in non_running_entries if entry.is_admin]) / total_duration * 100
                support_percentage = sum([entry.duration for entry in non_running_entries if entry.is_support]) / total_duration * 100
                total_hours = '{:02.0f}'.format(math.floor(total_duration / 3600))
                total_minutes = '{:02.0f}'.format(total_duration % 3600 / 60)
            except Exception as e:
                logger.log(e)
                sys.exit(1)

            if args.review:
                for entry in entries:
                    if entry.for_freshdesk:
                        freshdesk = '{} Freshdesk'.format(POSTED if entry.posted_freshdesk else NOT_POSTED)
                    else:
                        freshdesk = ''
                    logger.log('{entry} ({time}): {jira} JIRA {freshdesk}'.format(
                        entry=entry,
                        time=entry.logger.logable_time if not entry.running else 'running',
                        jira=POSTED if entry.posted_jira else NOT_POSTED,
                        freshdesk=freshdesk,
                    ))
                logger.log('Total time: {hours}h {mins}m ({admin:.0f}% admin, {support:.0f}% support)'.format(
                    hours=total_hours,
                    mins=total_minutes,
                    admin=admin_percentage,
                    support=support_percentage,
                ))
            else:
                freshdesk_count = jira_count = 0
                for entry in entries:
                    if entry.needs_posting:
                        logger.log('Posting {entry}: '.format(entry=entry), end='')
                    elif entry.running:
                        logger.log('{entry} is running, not posting.'.format(entry=entry), end='')
                    else:
                        logger.log('{entry} already posted.'.format(entry=entry), end='')
                    if not entry.posted_jira and not entry.running:
                        try:
                            jira.post(**entry.jira)
                            entry.mark_jira_posted()
                            jira_count += 1
                            logger.log('{posted} JIRA '.format(posted=POSTED), end='')
                        except Exception:
                            logger.log('{error} JIRA '.format(error=ERROR), end='')
                    if entry.for_freshdesk and not entry.posted_freshdesk and not entry.running:
                        try:
                            freshdesk.post(**entry.freshdesk)
                            entry.mark_freshdesk_posted()
                            freshdesk_count += 1
                            logger.log('{posted} Freshdesk'.format(posted=POSTED), end='')
                        except Exception:
                            logger.log('{error} Freshdesk'.format(error=ERROR), end='')
                    toggl.update_entry(entry)
                    logger.log('')
                if not total_duration and date.weekday() < 5:
                    logger.log('WARNING{}: Weekday with no time logged: {}-{:02}-{:02}'.format(ERROR, date.year, date.month, date.day))
                if freshdesk_enabled:
                    logger.log(
                        'Posted {jira} entries to JIRA and {freshdesk} entries to Freshdesk ({hours}h {mins}m) ({admin:.0f}% admin, {support:.0f}% support)'.format(
                            jira=jira_count,
                            freshdesk=freshdesk_count,
                            hours=total_hours,
                            mins=total_minutes,
                            admin=admin_percentage,
                            support=support_percentage,
                        ))
                else:
                    logger.log(
                        'Posted {jira} entries to JIRA ({hours}h {mins}m) ({admin:.0f}% admin, {support:.0f}% support)'.format(
                            jira=jira_count,
                            freshdesk=freshdesk_count,
                            hours=total_hours,
                            mins=total_minutes,
                            admin=admin_percentage,
                            support=support_percentage,
                        ))
    except Exception as e:
        logger.log(traceback.format_exc())
        sys.exit(1)


if __name__ == "__main__":
    main()
